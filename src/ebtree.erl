-module(ebtree).

-export([start/1]).
-export([stop/1]).
-export([think/2, think/3]).

-export([bt_if/1]).
-export([bt_or/1]).
-export([bt_and/1]).
-export([bt_loop/1, bt_loop/2]).
-export([bt_loop_or/1, bt_loop_or/2]).
-export([bt_loop_and/1, bt_loop_and/2]).
-export([bt_const/1]).
-export([bt_action/1]).

-type bt_input()       :: term().
-type bt_output()      :: term().
-type bt_result()      :: success | failure.
-type bt_blackboard()  :: dict:dict(term(), term()).
-type bt_function()    :: fun((bt_input(), bt_blackboard(), term()) -> bt_result()).
-type bt_node()        :: {bt_function(), term()}.
-type bt_children()    :: list(bt_node()).
-type bt_if_func()     :: fun((bt_input(), bt_blackboard()) -> boolean()).
-type bt_action_func() ::
	fun((bt_input(), bt_blackboard()) ->
		{bt_result() | running, bt_output(), bt_blackboard()} |
		{bt_result(), bt_blackboard()}).

-record(state, {
	root       = undefined  :: bt_node(),
	coro       = undefined  :: ecoro:ecoro() | undefined,
	blackboard = dict:new() :: bt_blackboard()
}).
-type state() :: #state{}.
-export_type([bt_node/0, state/0]).

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% start
%% ------------------------------------

-spec start(
	bt_node()
) -> state().

start(Root) ->
	#state{root = Root}.

%% ------------------------------------
%% stop
%% ------------------------------------

-spec stop(
	state()
) -> ok.

stop(#state{coro = Coro}) ->
	case ecoro:is_dead(Coro) of
		true  -> ok;
		false -> ecoro:shutdown(Coro)
	end.

%% ------------------------------------
%% think
%% ------------------------------------

-spec think(
	bt_input(), state()
) -> {output, bt_output(), state()} | {bt_result(), state()}.

think(Input, State) ->
	think(Input, success, State).

-spec think(
	bt_input(), bt_result(), state()
) -> {output, bt_output(), state()} | {bt_result(), state()}.

think(Input, LastResult,
	State = #state{
		root       = Root,
		coro       = Coro,
		blackboard = Blackboard
	}
) ->
	NewCoro = check_coro_start(Root, Coro),
	case ecoro:resume(NewCoro, {LastResult, Input, Blackboard}) of
		{true, {Output, NewBlackboard}} ->
			{output, Output, State#state{
				coro = NewCoro,
				blackboard = NewBlackboard}};
		{false, {Result, NewBlackboard}} ->
			{Result, State#state{
				coro = NewCoro,
				blackboard = NewBlackboard}}
	end.

%% ------------------------------------
%% bt_xxx
%% ------------------------------------

-spec bt_if(bt_if_func()) -> bt_node().
bt_if(Func) ->
	{fun bt_if_impl/3, Func}.

-spec bt_or(bt_children()) -> bt_node().
bt_or(Children) ->
	{fun bt_or_impl/3, Children}.

-spec bt_and(bt_children()) -> bt_node().
bt_and(Children) ->
	{fun bt_and_impl/3, Children}.

-spec bt_loop(bt_node()) -> bt_node().
bt_loop(Child) ->
	{fun bt_loop_impl/3, {infinity, Child}}.

-spec bt_loop(non_neg_integer(), bt_node()) -> bt_node().
bt_loop(N, Child) ->
	{fun bt_loop_impl/3, {N, Child}}.

-spec bt_loop_or(bt_children()) -> bt_node().
bt_loop_or(Children) ->
	bt_loop(bt_or(Children)).

-spec bt_loop_or(non_neg_integer(), bt_children()) -> bt_node().
bt_loop_or(N, Children) ->
	bt_loop(N, bt_or(Children)).

-spec bt_loop_and(bt_children()) -> bt_node().
bt_loop_and(Children) ->
	bt_loop(bt_and(Children)).

-spec bt_loop_and(non_neg_integer(), bt_children()) -> bt_node().
bt_loop_and(N, Children) ->
	bt_loop(N, bt_and(Children)).

-spec bt_const(bt_result()) -> bt_node().
bt_const(Result) ->
	{fun bt_const_impl/3, Result}.

-spec bt_action(bt_action_func()) -> bt_node().
bt_action(Func) ->
	{fun bt_action_impl/3, Func}.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% check_coro_start
%% ------------------------------------

-spec check_coro_start(
	bt_node(), ecoro:ecoro() | undefined
) -> ecoro:ecoro().

check_coro_start({Func,Param}, undefined) ->
	ecoro:start(fun({_LastResult, Input, Blackboard}) ->
		Func(Input, Blackboard, Param)
	end);

check_coro_start(Root, Coro) ->
	case ecoro:is_dead(Coro) of
		true  -> check_coro_start(Root, undefined);
		false -> Coro
	end.

%% ------------------------------------
%% bt_if_impl
%% ------------------------------------

-spec bt_if_impl(
	bt_input(), bt_blackboard(), bt_if_func()
) -> {bt_result(), bt_blackboard()}.

bt_if_impl(Input, Blackboard, Func) ->
	case Func(Input, Blackboard) of
		true  -> {success, Blackboard};
		false -> {failure, Blackboard}
	end.

%% ------------------------------------
%% bt_or_impl
%% ------------------------------------

-spec bt_or_impl(
	bt_input(), bt_blackboard(), bt_children()
) -> {bt_result(), bt_blackboard()}.

bt_or_impl(_Input, Blackboard, []) ->
	{failure, Blackboard};

bt_or_impl(Input, Blackboard, [{Func,Param}|Tail]) ->
	case Func(Input, Blackboard, Param) of
		{failure, NewBlackboard} ->
			bt_or_impl(Input, NewBlackboard, Tail);
		Result ->
			Result
	end.

%% ------------------------------------
%% bt_and_impl
%% ------------------------------------

-spec bt_and_impl(
	bt_input(), bt_blackboard(), bt_children()
) -> {bt_result(), bt_blackboard()}.

bt_and_impl(_Input, Blackboard, []) ->
	{success, Blackboard};

bt_and_impl(Input, Blackboard, [{Func,Param}|Tail]) ->
	case Func(Input, Blackboard, Param) of
		{success, NewBlackboard} ->
			bt_and_impl(Input, NewBlackboard, Tail);
		Result ->
			Result
	end.

%% ------------------------------------
%% bt_loop_impl
%% ------------------------------------

-spec bt_loop_impl(
	bt_input(), bt_blackboard(), {infinity | non_neg_integer(), bt_node()}
) -> {bt_result(), bt_blackboard()}.

bt_loop_impl(Input, Blackboard, {infinity, Node = {Func,Param}}) ->
	case Func(Input, Blackboard, Param) of
		{success, NewBlackboard} ->
			bt_loop_impl(Input, NewBlackboard, {infinity, Node});
		Result ->
			Result
	end;

bt_loop_impl(_Input, Blackboard, {0, _Node}) ->
	{success, Blackboard};

bt_loop_impl(Input, Blackboard, {N, Node = {Func,Param}}) ->
	case Func(Input, Blackboard, Param) of
		{success, NewBlackboard} ->
			bt_loop_impl(Input, NewBlackboard, {N - 1, Node});
		Result ->
			Result
	end.

%% ------------------------------------
%% bt_const_impl
%% ------------------------------------

-spec bt_const_impl(
	bt_input(), bt_blackboard(), bt_result()
) -> {bt_result(), bt_blackboard()}.

bt_const_impl(_Input, Blackboard, Result) ->
	{Result, Blackboard}.

%% ------------------------------------
%% bt_action_impl
%% ------------------------------------

-spec bt_action_impl(
	bt_input(), bt_blackboard(), bt_action_func()
) -> {bt_result(), bt_blackboard()}.

bt_action_impl(Input, Blackboard, Func) ->
	case Func(Input, Blackboard) of
		{running, Output, Blackboard2} ->
			case ecoro:yield({Output, Blackboard2}) of
				{success, NewInput, Blackboard3} ->
					bt_action_impl(NewInput, Blackboard3, Func);
				{failure, _NewInput, Blackboard3} ->
					{failure, Blackboard3}
			end;
		{success, Output, Blackboard2} ->
			case ecoro:yield({Output, Blackboard2}) of
				{Result, _NewInput, Blackboard3} ->
					{Result, Blackboard3}
			end;
		{failure, Output, Blackboard2} ->
			case ecoro:yield({Output, Blackboard2}) of
				{_Result, _NewInput, Blackboard3} ->
					{failure, Blackboard3}
			end;
		{Result, Blackboard2} ->
			{Result, Blackboard2}
	end.
