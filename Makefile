all: get_deps compile_all
dev: compile_self check_self test

clean:
	rebar clean

get_deps:
	rebar get-deps

test:
	rebar eunit

compile_all:
	rebar compile

compile_self:
	rebar compile skip_deps=true

check_self:
	dialyzer -r ebin

build_plt:
	dialyzer --build_plt --apps erts kernel stdlib deps/ecoro
